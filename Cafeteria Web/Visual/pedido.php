<?php
echo" <html lang='pt-br'>
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>

    <!-- Bootstrap CSS -->
    <link rel='stylesheet' href='node_modules/bootstrap/compiler/bootstrap.css'>
    <link rel='stylesheet' href='node_modules/bootstrap/compiler/style.css'>
    <link rel='stylesheet' type='text/css' href='style.css'>

  </head>
  <body id='fundo'>

    <div class='card' id='telaLogin'>
      <div class='card-body'>
        <form action='../Controle/TabelaAdd.php' method='POST'>
          <h2 class='text-center'> Faça seu pedido online </h2>
          <div class='form-group'>
            <label > Seu nome</label>
            <input type='text' name='nome' class='form-control' id='nome' aria-describedby='userHelp' placeholder='Digite seu nome de usuário' Required>
          </div>
          <div class='form-group'>
            <label>Opção</label>
            <input type='text' name='opcao' class='form-control' id='opcao' placeholder='Digite a opção desejada' Required>
          </div>
          <div class='form-group'>
            <label>Tipo de serviço</label>
            <input type='text' name='servico' class='form-control' id='servico' placeholder='Digite o serviço desejado' Required>
          </div>
          <div class='form-group'>
            <label>Endereço</label>
            <input type='text' name='local' class='form-control' id='local' placeholder='Digite o local desejado'>
          </div>
          <button type='submit' class='btn btn-outline-secondary btn-block'>Fazer pedido</button>
        </form>
        <a href='ProdutoServico.php'><button type='submit' class='btn btn-secondary btn-block'>Voltar</button></a>
      </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src='node_modules/jquery/dist/jquery.js'></script>
    <script src='node_modules/popper.js/dist/umd/popper.js'></script>
    <script src='node_modules/bootstrap/dist/js/bootstrap.js'></script>
  </body>
</html>";
?>