<?php
  session_start();
  if (!isset($_SESSION['nome'])) {
    header("Location: login.php");
  }

echo "<html lang='pt-br'>
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>

    <!-- Bootstrap CSS -->
    <link rel='stylesheet' href='node_modules/bootstrap/compiler/bootstrap.css'>
    <link rel='stylesheet' href='node_modules/bootstrap/compiler/style.css'>
    <link rel='stylesheet' type='text/css' href='style3.css'>

  </head>
  <body style='background-image: url(img/mq.jpg);' >
    <nav class='navbar navbar-expand-lg navbar-dark bg-dark'> 
      
        <a class='navbar-brand' href='ProdutoServico.php'><img src='img/lopi.png'></a>
        <a class='navbar-brand h1 mb-0' href='ProdutoServico.php' > CoffeeBreak </a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSite'>
          <span class='navbar-toggler-ícon'> </span>
        </button>
          <div class='collapse navbar-collapse' id='navbarSite'> 
            <ul class='navbar-nav ml-auto '>
            
              <li class='nav-item'>
                  <a class='nav-link' href='pedido.php'>Fazer Pedido Online</a>
              </li>
              <li class='nav-item'>
                  <a class='nav-link' href='../Controle/sair.php'> Sair </a>
               </li>
            </ul>
          </div>
    </nav>
  <div>
      <img src='img/8.jpg' class='img-fluid w-100 '>
    </div>
  
    <div class='jumbotron'>
  		<h1 class='display-3 text-center my-5'>Tipos de serviço</h1>
      <div class='container'>
      <div class='row my-5'>
        <div class='col-sm-6 col-md-4 mb-4'>
          <div class='card'>
            <img class='card-img-top' src='img/4.png'>
            <div  class='card-body text-center'>
              <h4 class='card-title'>Entrega</h4>
              <p class='card-text'> Iremos deixar seu café na sua casa.</p>
  
            </div>
          </div>
        </div>
        <div class='col-sm-12 col-md-4 mb-4'>
          <div class='card'>
            <img class='card-img-top' src='img/5.jpg'>
            <div  class='card-body text-center'>
              <h4 class='card-title'>Serviço</h4>
              <p class='card-text'> Conte com nossa rapidez para atende-los.</p>
            </div>
          </div>
        </div>
        <div class='col-sm-6 col-md-4 mb-4'>
          <div class='card'>
            <img class='card-img-top' src='img/5.jpg'>
            <div  class='card-body text-center'>
              <h4 class='card-title'>Buscar</h4>
              <p class='card-text'> Receba seu café na nossa cafeteria.</p>

            </div>
          </div>
        </div>
      </div>
  </div>
    <div class='jumbotron' >
      <h1 class='display-3 text-center'>Opções de café disponíveis</h1>
      <div class='container'>
      <div class='row my-5'>
        <div class='col-sm-6 col-md-4 mb-4'>
          <div class='card'>
            <img class='card-img-top' src='img/1.jpg'>
            <div  class='card-body text-center'>
              <h4 class='card-title'>Café Expresso</h4>
              <p class='card-text'> Feito em uma máquina que passa o café moído na pressão.</p>
  
            </div>
          </div>
        </div>
        <div class='col-sm-6 col-md-4 mb-4'>
          <div class='card'>
            <img class='card-img-top' src='img/2.jpg'>
            <div  class='card-body text-center'>
              <h4 class='card-title'>Capuccino</h4>
              <p class='card-text'>  Capuccino de verdade é o espresso junto com leite batido no vapor.</p>

            </div>
          </div>
        </div>
        <div class='col-sm-12 col-md-4 mb-4'>
          <div class='card'>
            <img class='card-img-top' src='img/0.jpg'>
            <div  class='card-body text-center'>
              <h4 class='card-title'>Macchiato</h4>
              <p class='card-text'> O café é quente e o leite é frio, e usam um tipo de creme de leite mais ralinho.</p>
            </div>
          </div>
        </div>

      </div>
    </div>
    </div>
    </div>
    <div>
      <img src='img/mo.jpg' class='img-fluid w-100 '>
    </div>
    <div class=''>
        <h1 class='display-3 text-center my-5'> Benefícios que o café possui </h1>
        <p class='text-center my-5' style='font-size: 25px; margin: 20px;'> Evita o câncer: Segundo o site Saúde Dica, a incidência do câncer de mama, cancro da faringe e oral, próstata, fígado e esofágico pode ser evitada com a ingestão de pelo menos uma xícara de café por dia. Por ser rico em antioxidantes listados acima, ajuda a evitar a degradação e alteração das células. Prevenindo, assim, o surgimento de mutações que podem resultar em tumores.</p> 
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src='node_modules/jquery/dist/jquery.js'></script>
    <script src='node_modules/popper.js/dist/umd/popper.js'></script>
    <script src='node_modules/bootstrap/dist/js/bootstrap.js'></script>
  </body>
</html>";

?>
