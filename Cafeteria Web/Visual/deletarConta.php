<?php
echo" <html lang='pt-br'>
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>

    <!-- Bootstrap CSS -->
    <link rel='stylesheet' href='node_modules/bootstrap/compiler/bootstrap.css'>
    <link rel='stylesheet' href='node_modules/bootstrap/compiler/style.css'>
    <link rel='stylesheet' type='text/css' href='style.css'>

  </head>
  <body id='fundo'>
    <div class='card' id='telaLogin'>
      <div class='card-body'>
        <form action='../Controle/apaProcessa.php' method='POST'>
          <h2 class='text-center'> Apagar conta </h2>
          <div class='form-group'>
            <label >Digite seu email</label>
            <input type='text' name='email' class='form-control' id='email' aria-describedby='userHelp' placeholder='Digite seu email' Required>
          </div>
          <div class='form-group'>
            <label>Digite sua senha</label>
            <input type='password' name='senha' class='form-control' id='senha' placeholder='Digite sua nova senha' Required>
          </div>
          <button type='submit' class='btn btn-outline-secondary btn-block'>Deletar</button>
        </form>
        <a href='login.php'><button type='submit' class='btn btn-secondary btn-block'>Voltar</button></a>
      </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src='node_modules/jquery/dist/jquery.js'></script>
    <script src='node_modules/popper.js/dist/umd/popper.js'></script>
    <script src='node_modules/bootstrap/dist/js/bootstrap.js'></script>
  </body>
</html>";
?>