<?php
	class Produto{
        private $idProduto;
        private $opcao;
        private $preco;
    
        public function getIdProduto(){
            return $this->idProduto;
        }
        public function setIdProduto($ip){
            $this->idProduto = $ip;
        }
        public function getOpcao(){
            return $this->opcao;
        }
        public function setOpcao($o){
            $this->opcao = $o;
        }
        public function getPreco(){
            return $this->preco;
        }
        public function setPreco($p){
            $this->preco = $p;
        }
    }
?>
