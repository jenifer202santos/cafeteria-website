<?php
	class Servico{
        private $idServico;
        private $tipoServico;
        
        public function getIdServico(){
            return $this->idServico;
        }
        public function setIdServico($is){
            $this->idServico = $is;
        }
        public function getTipoServico(){
            return $this->tipoServico;
        }
        public function setTipoServico($tp){
            $this->tipoServico = $tp;
        }
    }
?>
