    <?php
	class Usuario{
        private $nome;
        private $senha;
        private $email;

        public function getNome(){
            return $this->nome;
        }
        public function getSenha(){
            return $this->senha;
        }
        public function setNome($n){
            $this->nome = $n;
        }
        public function setSenha($s){
            $this->senha = $s;
        }
        public function getEmail(){
            return $this->email;
        }
        public function setEmail($m){
            $this->email = $m;
        }
    }
?>
