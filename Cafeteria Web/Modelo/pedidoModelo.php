<?php
	class Pedido{
        private $idPedido;
        private $idUser;
        private $idServico;
        private $idProduto;
        private $local;

        public function getIdUser(){
            return $this->idUser;
        }
        public function setIdUser($iu){
            $this->idUser = $iu;
        }
        public function getIdPedido(){
            return $this->idPedido;
        }
        public function setIdPedido($ip){
            $this->idPedido = $ip;
        }
        public function getIdServico(){
            return $this->idServico;
        }
        public function setIdServico($is){
            $this->idServico = $is;
        }
        public function getIdProduto(){
            return $this->idProduto;
        }
        public function setIdProduto($ipr){
            $this->idProduto = $ipr;
        }
        public function getLocal(){
            return $this->local;
        }
        public function setLocal($l){
            $this->local = $l;
        }
    }
?>
