<?php
require_once("conexaofut.php");
require_once("../Modelo/modelo.php");
    class UsuarioControle{
        function remover($usuario){
            try{
                $conexao = new Conexao();
                $email = $usuario->getEmail();
                $senha = $usuario->getSenha();
                $cmd = $conexao->getConexao()->prepare("DELETE FROM usuario WHERE email=:e AND senha=:s");
                $cmd->bindParam("e", $email);
                $cmd->bindParam("s", $senha);
                if($cmd->execute()){
                    return true;
                }else{
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro de PDO: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Usuario");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function inserir($usuario){
            try{
                $conexao = new Conexao();
                $nome = $usuario->getNome();
                $mail = $usuario->getEmail();
                $senha = $usuario->getSenha();
               
                $cmd = $conexao->getConexao()->prepare("INSERT INTO usuario(nome,email,senha) VALUES(:n,:e,:s);");
                $cmd->bindParam("n", $nome);
                $cmd->bindParam("e", $mail);
                $cmd->bindParam("s", $senha);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function login($model){
             try{
                $conexao = new Conexao();
                $nome = $model->getNome();
                $senha = $model->getSenha();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE nome = :n AND senha = :s;");
                $cmd->bindParam("n",$nome);
                $cmd->bindParam("s",$senha);
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS,"Usuario");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        
        }
        function update( $usuario ) {
       try {
                $conexao = new Conexao();
                $email = $usuario->getEmail();
                $senha = $usuario->getSenha();
                $cmd= $conexao->getConexao()->prepare("UPDATE usuario SET senha =:s WHERE email=:e");
                $cmd->bindParam("e", $email);
                $cmd->bindParam("s", $senha);
          if($cmd->execute()){
             if($cmd->rowCount() > 0){
                return true;
             } else {
                return false;
             }
          } else {
             return false;
          }
       } catch ( PDOException $excecao ) {
          echo $excecao->getMessage();
       }
    }

  
    
}
?>





