<?php
require_once("PedidoControle.php");
try{
    $usuario = new Usuario();
    $pedido = new Pedido();
    $produto = new Produto();
    $servico = new Servico();
    $usuario->setNome($_POST['nome']);
    $servico->setTipoServico($_POST['servico']);
    $pedido->setLocal($_POST['local']);
    $produto->setOpcao($_POST['opcao']);
    $control = new PedidoControle();
    if($control->inserire($pedido,$usuario,$produto,$servico)){
        header("Location: ../Visual/ProdutoServico.php");
    }
}catch(Exception $e){
    echo "<p>Erro: {$e->getMessage()}</p>";
}

?>
