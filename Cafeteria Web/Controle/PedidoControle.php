<?php  
require_once("conexaofut.php");
require_once("../Modelo/pedidoModelo.php");
require_once("../Modelo/produtoModelo.php");
require_once("../Modelo/servicoModelo.php");
require_once("../Modelo/modelo.php");
    class PedidoControle{     
        function inserire($pedido,$usuario,$produto,$servico){
            try{
                $conexao = new Conexao();
                $local = $pedido->getLocal();
                $nome = $usuario->getNome();
                $opcao = $produto->getOpcao();
                $tipoServico = $servico->getTipoServico();
               
                $cmd = $conexao->getConexao()->prepare("INSERT INTO pedido(local,idUser,idServico,idProduto) VALUES(:l,(SELECT idUser FROM usuario WHERE nome =:n ),(SELECT idServico FROM servico WHERE tipoServico = :tp),(SELECT idProduto FROM produto WHERE opcao = :o));");
                $cmd->bindParam("l",$local);
                $cmd->bindParam("n", $nome);
                $cmd->bindParam("tp", $tipoServico);
                $cmd->bindParam("o", $opcao);
                if($cmd->execute()){
                    if($cmd->rowCount() > 0){
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
  
        }
}
?>